from setuptools import setup, find_packages

PACKAGES = find_packages()

setup(name='zappa_secrets_manager',
      version='0.5',
      description='Enables seamless zappa integration with AWS Secrets '
                  'Manager while still allowing local development via '
                  'environment variables',
      url='https://bitbucket.org/benjhastings/zappa-secrets-manager',
      author='Ben Hastings',
      author_email='ben@agiletek.co.uk',
      license='MIT',
      packages=PACKAGES,
      install_requires=[
          'python-dotenv',
          'boto3',
      ],
      zip_safe=False)
